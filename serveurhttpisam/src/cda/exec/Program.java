package cda.exec;

import cda.http.server.CdaServer;

public class Program {
	public static void main(String[] args) {
		new CdaServer(8888).start();
	}
}
