package cda.http.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class ServeurAuxiliaire implements Runnable {
	private final Socket client;

	public ServeurAuxiliaire(Socket c) {
		this.client = c;
	}

	@Override
	public void run() {
		try {
			InputStream clientIn = client.getInputStream();
			OutputStream clientOut = client.getOutputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));

			String uneLigne = br.readLine();
			System.out.println(uneLigne);
			String[] requeteTab = uneLigne.split(" ");
			if ("get".equalsIgnoreCase(requeteTab[0])) {
				
				if( !(new File("C:\\\\Users\\\\tnk976\\\\"+requeteTab[1].substring(1))).exists()) {
					clientOut.write(("HTTP/1.1 " + CdaStatus.ERROR.getCode() + " " + CdaStatus.ERROR.getDescription() + "\n").getBytes());
					clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
					clientOut.write("\n".getBytes());
					clientOut.write((CdaStatus.ERROR.getCode()+ " "+CdaStatus.ERROR.getDescription()).getBytes() );
					
				}else {
					clientOut.write(("HTTP/1.1 " + CdaStatus.OK.getCode() + " " + CdaStatus.OK.getDescription() + "\n").getBytes());
					clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
					clientOut.write("\n".getBytes());
					System.out.println(requeteTab[1].substring(1));
					try {
						File fichier = new File("C:\\Users\\tnk976\\"+requeteTab[1].substring(1));
						BufferedReader monBuffer = new BufferedReader(new InputStreamReader(new FileInputStream(fichier)));
						String st;
						while ((st = monBuffer.readLine()) != null) {
							clientOut.write(st.getBytes());
						}
						
					}catch(Exception e) {
						
					}
				}

			


			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				this.client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
