package cda.http.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class ServeurAuxiliaire implements Runnable {
	private final Socket client;

	public ServeurAuxiliaire(Socket c) {
		this.client = c;
	}

	@Override
	public void run() {
		try {
			InputStream clientIn = client.getInputStream();
			OutputStream clientOut = client.getOutputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));

			String uneLigne = br.readLine();
			System.out.println(uneLigne);
			String[] requeteTab = uneLigne.split(" ");
			if ("get".equalsIgnoreCase(requeteTab[0])) {
				clientOut.write(
						("HTTP/1.1 " + CdaStatus.OK.getCode() + " " + CdaStatus.OK.getDescription() + "\n").getBytes());
				clientOut.write("content-type:text/html; charset=UTF-8\n".getBytes());
				clientOut.write("\n".getBytes());
				clientOut.write("<html><body style='color:red'><b>coucou</b></body></html>".getBytes());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				this.client.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
